package MSim4;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Ian Shaw <ishaw@tuta.io>
 */

public class BuyerList {
    List<Buyer> buyerList = new ArrayList<>();
    
    public BuyerList(){
    }

    public void addBuyer(Integer MAX_PRICE, Integer demandUnits){
        this.buyerList.add(new Buyer(MAX_PRICE, demandUnits, this.buyerList.size()));
    }
    
    public Buyer getBuyer(Integer i){
        return this.buyerList.get(i);
    }
    
    public Integer getBuyerListSize(){
        return this.buyerList.size();
    }

    public void sortBuyers() {
        Collections.sort(this.buyerList);
        Collections.reverse(this.buyerList);     // orders high to low bid prices
    }

    @Override
    public String toString(){
        String agentListDetails=new String();
        if(!this.buyerList.isEmpty()){
            agentListDetails+=String.format("%-12s%-13s%-11s%-3s\n","REFERENCE","MAX (BID)","DEMAND","RISK STRATEGY");
            for(Buyer b : this.buyerList)
                agentListDetails += b.toString() + "\n";
        }
        else
            agentListDetails+="No buyers have been registered on this market.";
        return agentListDetails;        
    }      

    public void restoreAll() {
        for(Buyer b : buyerList) {
            b.setDemandUnits(b.getOriginalUnits());
            b.setMarketStrategy();
        }
        this.sortBuyers();
    }

}
