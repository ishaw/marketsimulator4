package MSim4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Ian Shaw <ishaw@tuta.io>
 */

public class SellerList {
    List<Seller> sellerList = new ArrayList<>();
    
    public SellerList(){
    }

    public void addSeller(Integer MIN_PRICE, Integer supplyUnits){
        this.sellerList.add(new Seller(MIN_PRICE, supplyUnits, this.sellerList.size()));
    }
   
    public Seller getSeller(Integer i){
        return this.sellerList.get(i);
    }
    
    public Integer getSellerListSize(){
        return this.sellerList.size();
    }
    
    public void sortSellers() {
        Collections.sort(this.sellerList);
    }
    
    public Seller getHighestSeller(){
        return Collections.max(this.sellerList);
    }
    
    public Seller getLowestSeller(){
        return Collections.min(this.sellerList);
    }
    
    @Override
    public String toString(){
        String agentListDetails=new String();
        if(!this.sellerList.isEmpty()){
            agentListDetails+=String.format("%-12s%-13s%-11s%-3s\n","REFERENCE","MIN (BID)","SUPPLY","RISK STRATEGY");
            for(Seller s : this.sellerList)
                agentListDetails += s.toString() + "\n";
        }
        else
            agentListDetails+="No sellers have been registered on this market.";
        return agentListDetails;        
    }      

    public void restoreAll() {
        for(Seller s : sellerList) {
            s.setSupplyUnits(s.getOriginalUnits());
            s.setMIN_PRICE(s.getOriginalPrice());
            s.setMarketStrategy();
        }
    }
   
}
