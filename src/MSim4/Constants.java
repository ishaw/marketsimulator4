
package MSim4;

/**
 * @author Ian Shaw <ishaw@tuta.io>
 */

public class Constants {

    private Constants() {} // stop this class from ever being instantiated
    
    public static final Integer LENGTH_OF_NEGOTIATION = 10;      // number of steps in the negotiation
    public static final Integer MAX_TRADE_SIZE = 300;            // most units that can be traded per round
    public static final Integer MARKET_PRICE_PROXIMITY_THRESHOLD = 2; // how close prices have to be to qualify as a resolved market price
    public static final Integer RESOURCE_COST = 30;                    // initial cost of resources for generators
    public static final Integer MAX_MARKET_ROUND_THRESHOLD = 1000;      // absolute maximum time the market will run for
}
