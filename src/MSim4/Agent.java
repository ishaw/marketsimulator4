package MSim4;

/**
 * @author Ian Shaw <ishaw@tuta.io>
 */

public class Agent implements Comparable<Agent>{
    private final String ref;
    private Integer marketBidPrice;
    private final double risk;

    public Agent(String ref){
        this.ref = ref;
        this.risk = 0.5 + (Math.random() * 0.5); // lower number, higher risk tolerance
    }
    
    public String getRef(){
        return this.ref;
    }

    public Integer getMarketBidPrice() {
        return marketBidPrice;
    }

    public void setMarketBidPrice(Integer marketBidPrice) {
        this.marketBidPrice = marketBidPrice;
    }

    public double getRisk() {
        return risk;
    }                    
    
    public void updateBidPrice(Integer marketPrice) {
        this.marketBidPrice = (marketPrice + this.marketBidPrice) / 2; // simple approach to consensus price
    }
    
    @Override
    public String toString(){
        return String.format("%-12s", ref);
    }      

    @Override
    public int compareTo(Agent agent) {
        return this.marketBidPrice.compareTo(agent.marketBidPrice);
    }
    
}
