package MSim4;

/**
 * @author Ian Shaw <ishaw@tuta.io>
 */

public class Seller extends Agent {
    private int MIN_PRICE;
    private Integer supplyUnits;
    private final Integer originalUnits;
    private final Integer originalPrice;
    private Integer moneyMade;
    
    public Seller(Integer MIN_PRICE, Integer supplyUnits, Integer numberOfSellers){
        super("S" + String.valueOf(numberOfSellers));
        this.MIN_PRICE = MIN_PRICE + Constants.RESOURCE_COST;
        this.supplyUnits = supplyUnits;
        this.originalUnits = supplyUnits;
        this.originalPrice = MIN_PRICE;
        this.moneyMade = 0;
        this.setMarketStrategy();
    }
    
    public Integer bidPrice(){
        // bid price strategy - not the same, necessarily, as getMIN_PRICE
        // return this.MIN_PRICE; // very desperate to sell
        
        return super.getMarketBidPrice();
        
    }

    public int getMIN_PRICE() {
        return MIN_PRICE;
    }

    public void setMIN_PRICE(int MIN_PRICE) {
        this.MIN_PRICE = MIN_PRICE;
    }

    public Integer getOriginalPrice() {
        return originalPrice;
    }
    
    public Integer bidAmount(){
        // bid amount strategy - not the same, necessarily, as getSupplyUnits
        return this.supplyUnits; // trying to sell out
    }
    
    public Integer getSupplyUnits(){
        return this.supplyUnits;
    }
    
    public void setSupplyUnits(Integer units){
        this.supplyUnits = units;
    }
    
    public Integer getMoneyMade(){
        return this.moneyMade;
    }
    
    public void addMoneyMade(Integer money){
        this.moneyMade += money;
    }

    public void setMarketStrategy() {
        super.setMarketBidPrice((int) ((double)this.MIN_PRICE / super.getRisk()));
    }

    public void shockPrice(Integer shockIncrement){
        this.MIN_PRICE += shockIncrement;
        this.setMarketStrategy();
    }
    
    public Integer getOriginalUnits() {
        return originalUnits;
    }
    
    @Override
    public String toString(){
        String agentDetails = super.toString();
        agentDetails += String.format("%-13s%-11s%.2f", this.MIN_PRICE + " (" + super.getMarketBidPrice() + ")",supplyUnits,super.getRisk());
        if(this.supplyUnits < this.originalUnits)
            agentDetails += " Profit: " + (this.moneyMade / (this.originalUnits - this.supplyUnits));
        return agentDetails;
    }    
}
