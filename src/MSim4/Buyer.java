package MSim4;

/**
 * @author Ian Shaw <ishaw@tuta.io>
 */

public class Buyer extends Agent {
    private final int MAX_PRICE;
    private Integer demandUnits;
    private final Integer originalUnits;
    private Integer moneySpent;
    
    public Buyer(int MAX_PRICE, Integer demandUnits, Integer numberOfBuyers){
        super("B" + String.valueOf(numberOfBuyers));
        this.MAX_PRICE = MAX_PRICE;
        this.demandUnits = demandUnits;
        this.originalUnits = demandUnits;
        this.moneySpent = 0;
        this.setMarketStrategy();
    }

    public Integer bidPrice(){
        // bid price strategy - not the same, necessarily, as getMAX_PRICE
        // return this.MAX_PRICE; // very desperate to buy
        
        return super.getMarketBidPrice();
        
    }

    public int getMAX_PRICE() {
        return MAX_PRICE;
    }

    public Integer getOriginalUnits() {
        return originalUnits;
    }
    
    public Integer bidAmount(){
        // bid amount strategy - not the same, necessarily, as getDemandUnits
        return this.demandUnits; // trying to satisfy all demand
    }
    
    public Integer getDemandUnits(){
        return this.demandUnits;
    }
    
    public void setDemandUnits(Integer units){
        this.demandUnits = units;
    }
    
    public Integer getMoneySpent(){
        return this.moneySpent;
    }
    
    public void addMoneySpent(Integer money){
        this.moneySpent += money;
    }

    public void setMarketStrategy() {
        super.setMarketBidPrice((int) ((double)this.MAX_PRICE * super.getRisk()));
    }
    
    @Override
    public String toString(){
        String agentDetails = super.toString();
        agentDetails += String.format("%-13s%-11s%.2f",MAX_PRICE + " (" + super.getMarketBidPrice() + ")",demandUnits,super.getRisk());
        if(demandUnits < originalUnits)
            agentDetails += "Cost: " + (moneySpent / (originalUnits - demandUnits));
        return agentDetails;
    }    
}
