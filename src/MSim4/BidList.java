package MSim4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Ian Shaw <ishaw@tuta.io>
 */

public class BidList {

    List<Bid> bidList = new ArrayList<>();

    public BidList() {
    }

    public void addBid(Integer bidPrice, Buyer buyer) {
        this.bidList.add(new Bid(bidPrice, buyer));
    }

    public Bid getBid(Integer i) {
        return this.bidList.get(i);
    }

    public Integer getBidListSize() {
        return this.bidList.size();
    }

    public void deleteBid(Buyer buyerToDelete) {
        Iterator<Bid> it = bidList.iterator();
        while (it.hasNext()) {
            Bid b = it.next();
            if (b.getBuyer().equals(buyerToDelete)) {
                it.remove();
            }
        }
    }

    public void clearAllBids() {
        this.bidList.clear();
    }
    
    public boolean marketPriceNotFound(SellerList sellerList) {

        boolean marketPriceNotFound = true;
        if (bidList.isEmpty()) {
            marketPriceNotFound = false;
        } else {

            Integer highprice = bidList.get(0).getBuyer().getMarketBidPrice();
            Integer lowprice = bidList.get(0).getBuyer().getMarketBidPrice();

            for (Bid b : bidList) {
                if (highprice < b.getBuyer().getMarketBidPrice()) {
                    highprice = b.getBuyer().getMarketBidPrice();
                } else if (lowprice > b.getBuyer().getMarketBidPrice()) {
                    lowprice = b.getBuyer().getMarketBidPrice();
                }
            }

            if (highprice < sellerList.getHighestSeller().bidPrice()) {
                highprice = sellerList.getHighestSeller().bidPrice();
            }

            if (lowprice > sellerList.getLowestSeller().bidPrice()) {
                lowprice = sellerList.getLowestSeller().bidPrice();
            }

            if ((highprice - lowprice) <= Constants.MARKET_PRICE_PROXIMITY_THRESHOLD) {
                marketPriceNotFound = false;
            }
        }
        return marketPriceNotFound;
    }

    @Override
    public String toString() {
        String bidListDetails = new String();
        if (!this.bidList.isEmpty()) {
            for (Bid b : bidList) {
                bidListDetails += b.toString();
            }
        } else {
            bidListDetails += "No bids have been registered on this market.";
        }
        return bidListDetails;
    }

}
