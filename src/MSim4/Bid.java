package MSim4;

/**
 * @author Ian Shaw <ishaw@tuta.io>
 */

public class Bid {
    private Integer bidPrice;
    private Boolean bidAccepted;
    private final Buyer buyer;
    private Integer round;
    
    public Bid(Integer bidPrice, Buyer buyer){
        this.bidPrice = bidPrice;
        this.buyer = buyer;
        this.bidAccepted = false;
        this.round = 1;
    }

    public Integer getRound() {
        return round;
    }

    public void setRound(Integer round) {
        this.round = round;
    }
    
    public Integer getBidPrice(){
        return this.bidPrice;
    }
    
    public void setBidPrice(Integer bidPrice){
        this.bidPrice = bidPrice;        
    }
    
    public Boolean getBidAccepted(){
        return this.bidAccepted;
    }
    
    public void setBidAccepted(Boolean bid){
        this.bidAccepted = bid;
    }
    
    public Buyer getBuyer(){
        return this.buyer;
    }
    
    @Override
    public String toString(){
        String bidDetails = "Bid Buyer Ref: "+this.buyer.getRef()+", Bid Price: "+this.bidPrice+", Bid Accepted: "+this.bidAccepted;
        return bidDetails;
    }
    
}
