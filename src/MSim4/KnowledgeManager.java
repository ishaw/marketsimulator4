package MSim4;

import java.util.ArrayList;
import java.util.List;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 * @author Ian Shaw <ishaw@tuta.io>
 */

public class KnowledgeManager {
    
    List<BidEvent> bidEvents = new ArrayList<>();
    
    public KnowledgeManager() {
    }
    
    public void addBidEvent(String agentRef, Integer bidValue, Integer round) {
        this.bidEvents.add(new BidEvent(agentRef, bidValue, round));
    }

    public DefaultCategoryDataset createChart() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for(BidEvent b : bidEvents)
            dataset.setValue(b.getBidValue(), b.getBidAgentRef(), b.getBidRound());    
        return dataset;
    }

    public void purgeData() {
        this.bidEvents.clear();       
    }
    
    @Override
    public String toString() {
        String bidEventString = new String();
        for(BidEvent b : bidEvents)
            bidEventString += b.toString() + "\n";
        return bidEventString;
    }

}
